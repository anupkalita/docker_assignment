CREATE DATABASE IF NOT EXISTS details;

USE details;

CREATE TABLE IF NOT EXISTS person( id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(50), age INT);

CREATE USER 'anup'@'localhost' IDENTIFIED BY 'kinddevil';
       GRANT ALL PRIVILEGES ON *.* TO 'anup'@'localhost';
        FLUSH PRIVILEGES;



